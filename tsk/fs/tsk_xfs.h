/*
** The Sleuth Kit
**
** Brian Carrier [carrier <at> sleuthkit [dot] org]
** Copyright (c) 2003-2011 Brian Carrier.  All rights reserved
**
** TASK
** Copyright (c) 2002 Brian Carrier, @stake Inc.  All rights reserved
**
** This software is distributed under the Common Public License 1.0
*/

/*
 * Contains the structures and function APIs for XFSFS file system support.
 */

#ifndef _TSK_XFSXFS_H
#define _TSK_XFSXFS_H

#ifdef __cplusplus
extern "C" {
#endif

/*
** Constants
*/

/* Magic Numbers */
#define XFS_SB_MAGIC            0x58465342  // XFSB (0x0~)
#define XFS_DIR3_BLOCK_MAGIC    0x58444233  // XDB3 : version 5
/*
** Constants
*/

#define XFS_FILE_CONTENT_LEN    336
#define XFSLABEL_MAX            12
#define XFS_MAXNAMLEN           255

/* inode core file types */
// #define XFS_DIR3_FT_UNNOWN      0200000 // unkown inode type. This should never appear on disk.
// #define XFS_DIR3_FT_WHT      0160000 // Entry points to an overlayfs whiteout file. This has never appeared on disk.
#define XFS_DIR3_FT_SOCK        0xC
#define XFS_DIR3_FT_SYMLNK      0xA
#define XFS_DIR3_FT_REG_FILE    0x8
#define XFS_DIR3_FT_BLKDEV      0x6
#define XFS_DIR3_FT_DIR         0x4
#define XFS_DIR3_FT_CHRDEV      0x2
#define XFS_DIR3_FT_FIFO        0x1

/* file permission for inode core */
#define XFS_IN_ISUID   0004000
#define XFS_IN_ISGID   0002000
#define XFS_IN_ISVTX   0001000
#define XFS_IN_IRUSR   0000400
#define XFS_IN_IWUSR   0000200
#define XFS_IN_IXUSR   0000100
#define XFS_IN_IRGRP   0000040
#define XFS_IN_IWGRP   0000020
#define XFS_IN_IXGRP   0000010
#define XFS_IN_IROTH   0000004
#define XFS_IN_IWOTH   0000002
#define XFS_IN_IXOTH   0000001

/* file types for inode core */
#define XFS_IN_FMT  0170000
#define XFS_IN_SOCK 0140000
#define XFS_IN_LNK  0120000
#define XFS_IN_REG  0100000
#define XFS_IN_BLK  0060000
#define XFS_IN_DIR  0040000
#define XFS_IN_CHR  0020000
#define XFS_IN_FIFO 0010000

/* file types for directory entry */
#define XFS_DE_UNKNOWN    0
#define XFS_DE_REG        1
#define XFS_DE_DIR        2
#define XFS_DE_CHR        3
#define XFS_DE_BLK        4
#define XFS_DE_FIFO       5
#define XFS_DE_SOCK       6
#define XFS_DE_LNK        7
#define XFS_DE_MAX        8

/*
** Super Block
*/
        typedef struct {
            uint8_t sb_magicnum[4];
            uint8_t sb_blocksize[4];
            uint8_t sb_dblocks[8];
            uint8_t sb_rblocks[8];
            uint8_t sb_rextents[8];
            uint8_t sb_uuid[16];
            uint8_t sb_logstart[8];
            uint8_t sb_rootino[8];
            uint8_t sb_rbmino[8];
            uint8_t sb_rsumino[8];
            uint8_t sb_rextsize[4];
            uint8_t sb_agblocks[4];
            uint8_t sb_agcount[4];
            uint8_t sb_rbmblocks[4];
            uint8_t sb_logblocks[4];
            uint8_t sb_versionnum[2];
            uint8_t sb_sectsize[2];
            uint8_t sb_inodesize[2];
            uint8_t sb_inopblock[2];
            char sb_fname[XFSLABEL_MAX];
            uint8_t sb_blocklog;
            uint8_t sb_sectlog;
            uint8_t sb_inodelog;
            uint8_t sb_inopblog;        // used for absolute inode number format
            uint8_t sb_agblklog;        // used for absolute inode number format
            uint8_t sb_rextslog;        
            uint8_t sb_inprogress;
            uint8_t sb_imax_pct;
            uint8_t sb_icount[8];
            uint8_t sb_ifree[8];
            uint8_t sb_fdblocks[8];
            uint8_t sb_frextents[8];
            uint8_t sb_uquotino[8];
            uint8_t sb_gquotino[8];
            uint8_t sb_qflags[2];
            uint8_t sb_flags;
            uint8_t sb_shared_vn;
            uint8_t sb_inoalignmt[4];
            uint8_t sb_unit[4];
            uint8_t sb_width[4];
            uint8_t sb_dirblklog;
            uint8_t sb_logsectlog;
            uint8_t sb_logsectsize[2];
            uint8_t sb_logsunit[4];
            uint8_t sb_features2[4];
            uint8_t sb_bad_features2[4];
            /* version 5 superblock fields start here */
            uint8_t sb_features_compat[4];
            uint8_t sb_features_ro_compat[4];
            uint8_t sb_features_incompat[4];
            uint8_t sb_features_log_incompat[4];
            uint8_t sb_crc[4];
            uint8_t sb_spino_align[8];
            uint8_t sb_pquotino[8];
            uint8_t sb_lsn[8];
            uint8_t sb_meta_uuid[16];
            uint8_t sb_rrmapino[8];

        } xfs_sb;

/* structures for Block directory end */    
        typedef struct {
            uint8_t di_magic[2];
            uint8_t di_mode[2];
            uint8_t di_version;
            uint8_t di_format;
            uint8_t di_onlink[2];
            uint8_t di_uid[4];
            uint8_t di_gid[4];
            uint8_t di_nlink[4];
            uint8_t di_projid[2];
            uint8_t di_projid_hi[2];
            uint8_t di_pad[6];
            uint8_t di_flushiter[2];
            uint8_t di_atime[8];
            uint8_t di_mtime[8];
            uint8_t di_ctime[8];
            uint8_t di_size[8];
            uint8_t di_nblocks[8];
            uint8_t di_extsize[4];
            uint8_t di_nextents[4];
            uint8_t di_anextents[2];
            uint8_t di_forkoff;
            uint8_t di_aformat;
            uint8_t di_dmevmask[4];
            uint8_t di_dmstate[2];
            uint8_t di_flags[2];
            uint8_t di_gen[4];
            /* di_next_unlinked is the only non-core field in the old dinode */
            uint8_t di_next_unlinked[4];
            /* version 5 filesystem (inode version 3) fields start here */
            uint8_t di_crc[4];
            uint8_t di_changecount[8];
            uint8_t di_lsn[8];
            uint8_t di_flags2[8];
            uint8_t di_cowextsize[4];
            uint8_t di_pad2[12];
            uint8_t di_crtime[8];
            uint8_t di_ino[8];
            uint8_t di_uuid[16];
            //xfs_extent di_extent[10];
            uint8_t di_datafork[21][16];
        } xfs_dinode_core;

        /* di_format */
        typedef enum xfs_dinode_fmt {
            XFS_DINODE_FMT_DEV,
            XFS_DINODE_FMT_LOCAL,
            XFS_DINODE_FMT_EXTENTS,
            XFS_DINODE_FMT_BTREE,
            XFS_DINODE_FMT_UUID,
            XFS_DINODE_FMT_RMAP,
        } xfs_dinode_fmt_t;

        /* br_state field uses the following enum declaration */
        typedef enum {
            XFS_EXT_NORM,
            XFS_EXT_UNWRITTEN,
            XFS_EXT_INVALID
       } xfs_exntst_t;

        /*
         * Structure of an XFSfs file system handle.
         */
        typedef struct {
            TSK_FS_INFO fs_info;    /* super class */
            xfs_sb *fs;             /* super block */
            uint16_t inode_size;    /* size of each inode */
        } XFS_INFO;

// for 8-byte alignment in block directory
#define XFS_DIRSIZ_lcl(len) \
    ((cursor+(8-cursor%8)) - cursor < 2 ? cursor+(8-cursor%8)+8 : cursor+(8-cursor%8))

#ifdef __cplusplus
}
#endif
#endif
