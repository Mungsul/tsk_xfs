/*
** The Sleuth Kit 
**
** Brian Carrier [carrier <at> sleuthkit [dot] org]
** Copyright (c) 2006-2011 Brian Carrier, Basis Technology.  All Rights reserved
** Copyright (c) 2004-2005 Brian Carrier.  All rights reserved 
**
**
** This software is distributed under the Common Public License 1.0
**
*/

#include "tsk_fs_i.h"
#include "tsk_xfs.h"

/* xfs_dinode_load - look up disk inode & load into xfs_dinode_core structure
 * @param xfs A xfs file system information structure
 * @param dino_inum Metadata address
 * @param dino_buf The buffer to store the block in (must be size of xfs->inode_size or larger)
 *
 * return 1 on error and 0 on success
 * */

static uint8_t
xfs_dinode_load(XFS_INFO * xfs, TSK_INUM_T dino_inum,
    xfs_dinode_core * dino_buf)
{
    int i;
    TSK_OFF_T addr;
    ssize_t cnt;
    TSK_INUM_T rel_inum;
    TSK_FS_INFO *fs = (TSK_FS_INFO *) & xfs->fs_info;

    /*
     * Sanity check.
     * Use last_num-1 to account for virtual Orphan directory in last_inum.
     */
    if ((dino_inum < fs->first_inum)) {
        tsk_error_reset();
        tsk_error_set_errno(TSK_ERR_FS_INODE_NUM);
        tsk_error_set_errstr("xfs_dinode_load: address: %" PRIuINUM,
            dino_inum);
        return 1;
    }

    if (dino_buf == NULL) {
        tsk_error_reset();
        tsk_error_set_errno(TSK_ERR_FS_ARG);
        tsk_error_set_errstr("xfs_dinode_load: dino_buf is NULL");
        return 1;
    }

    addr = dino_inum * xfs->inode_size;
    cnt = tsk_fs_read(fs, addr, (char *) dino_buf, xfs->inode_size);
    
    if(tsk_verbose) {
        tsk_fprintf(stderr, "inode core: \n");
        unsigned char *cur = (unsigned char *)dino_buf;
        for(int i=0;i<xfs->inode_size;i++){
            tsk_fprintf(stderr, "%02X ", cur[i]);
            if(i%16==15) tsk_fprintf(stderr, "\n");
        }
    }

    if (cnt != xfs->inode_size) {
        if (cnt >= 0) {
            tsk_error_reset();
            tsk_error_set_errno(TSK_ERR_FS_READ);
        }
        tsk_error_set_errstr2("xfs_dinode_load: Inode %" PRIuINUM
            " from %" PRIuOFF, dino_inum, addr);
        return 1;
    }


    if(tsk_verbose) {
        tsk_fprintf(stderr, "dinode number = %" PRIuINUM "\n", dino_inum);
        tsk_fprintf(stderr, "dinode magic = %"PRIx16"\n", 
                tsk_getu16(fs->endian,dino_buf->di_magic));

        if(dino_buf->di_format == XFS_DINODE_FMT_LOCAL)
            tsk_fprintf(stderr, "short from directory\n");
        else if(dino_buf->di_format == XFS_DINODE_FMT_EXTENTS)
            tsk_fprintf(stderr, "data fork type: extent\n");
        else if(dino_buf->di_format == XFS_DINODE_FMT_BTREE)
            tsk_fprintf(stderr, "data fork type: B+tree\n");
    }

    return 0;
}


/* xfs_dinode_copy - copy cached disk inode into generic inode
 *
 * returns 1 on error and 0 on success
 * */
static uint8_t
xfs_dinode_copy(XFS_INFO * xfs, TSK_FS_META * fs_meta,
    TSK_INUM_T inum, const xfs_dinode_core * dino_buf)
{
    int i;
    TSK_FS_INFO *fs = (TSK_FS_INFO *) & xfs->fs_info;
    xfs_sb *sb = xfs->fs;
    TSK_INUM_T ibase = 0;


    if (dino_buf == NULL) {
        tsk_error_reset();
        tsk_error_set_errno(TSK_ERR_FS_ARG);
        tsk_error_set_errstr("xfs_dinode_copy: dino_buf is NULL");
        return 1;
    }

    fs_meta->attr_state = TSK_FS_META_ATTR_EMPTY;
    if (fs_meta->attr) {
        tsk_fs_attrlist_markunused(fs_meta->attr);
    }

    // set the type
    switch (tsk_getu16(fs->endian, dino_buf->di_mode) >> 12) {
        case XFS_DIR3_FT_REG_FILE: fs_meta->type = TSK_FS_META_TYPE_REG; break;
        case XFS_DIR3_FT_DIR: fs_meta->type = TSK_FS_META_TYPE_DIR; break;
        case XFS_DIR3_FT_SOCK: fs_meta->type = TSK_FS_META_TYPE_SOCK; break;
        case XFS_DIR3_FT_SYMLNK: fs_meta->type = TSK_FS_META_TYPE_LNK; break;
        case XFS_DIR3_FT_BLKDEV: fs_meta->type = TSK_FS_META_TYPE_BLK; break;
        case XFS_DIR3_FT_CHRDEV: fs_meta->type = TSK_FS_META_TYPE_CHR; break;
        case XFS_DIR3_FT_FIFO: fs_meta->type = TSK_FS_META_TYPE_FIFO; break;
        default: fs_meta->type = TSK_FS_META_TYPE_UNDEF; break;
    }
   
    // set the mode
    fs_meta->mode = 0;
    if (tsk_getu16(fs->endian, dino_buf->di_mode) & XFS_IN_ISUID)
        fs_meta->mode |= TSK_FS_META_MODE_ISUID;
    if (tsk_getu16(fs->endian, dino_buf->di_mode) & XFS_IN_ISGID)
        fs_meta->mode |= TSK_FS_META_MODE_ISGID;
    if (tsk_getu16(fs->endian, dino_buf->di_mode) & XFS_IN_ISVTX)
        fs_meta->mode |= TSK_FS_META_MODE_ISVTX;

    if (tsk_getu16(fs->endian, dino_buf->di_mode) & XFS_IN_IRUSR)
        fs_meta->mode |= TSK_FS_META_MODE_IRUSR;
    if (tsk_getu16(fs->endian, dino_buf->di_mode) & XFS_IN_IWUSR)
        fs_meta->mode |= TSK_FS_META_MODE_IWUSR;
    if (tsk_getu16(fs->endian, dino_buf->di_mode) & XFS_IN_IXUSR)
        fs_meta->mode |= TSK_FS_META_MODE_IXUSR;

    if (tsk_getu16(fs->endian, dino_buf->di_mode) & XFS_IN_IRGRP)
        fs_meta->mode |= TSK_FS_META_MODE_IRGRP;
    if (tsk_getu16(fs->endian, dino_buf->di_mode) & XFS_IN_IWGRP)
        fs_meta->mode |= TSK_FS_META_MODE_IWGRP;
    if (tsk_getu16(fs->endian, dino_buf->di_mode) & XFS_IN_IXGRP)
        fs_meta->mode |= TSK_FS_META_MODE_IXGRP;

    if (tsk_getu16(fs->endian, dino_buf->di_mode) & XFS_IN_IROTH)
        fs_meta->mode |= TSK_FS_META_MODE_IROTH;
    if (tsk_getu16(fs->endian, dino_buf->di_mode) & XFS_IN_IWOTH)
        fs_meta->mode |= TSK_FS_META_MODE_IWOTH;
    if (tsk_getu16(fs->endian, dino_buf->di_mode) & XFS_IN_IXOTH)
        fs_meta->mode |= TSK_FS_META_MODE_IXOTH;

    fs_meta->nlink = tsk_getu32(fs->endian, dino_buf->di_nlink);
    fs_meta->size = tsk_getu64(fs->endian, dino_buf->di_size);
    fs_meta->addr = inum;

    fs_meta->uid = tsk_getu32(fs->endian, dino_buf->di_uid);
    fs_meta->gid = tsk_getu32(fs->endian, dino_buf->di_gid);
    fs_meta->mtime = tsk_getu32(fs->endian, dino_buf->di_mtime);
    fs_meta->atime = tsk_getu32(fs->endian, dino_buf->di_atime);    
    fs_meta->ctime = tsk_getu32(fs->endian, dino_buf->di_ctime);    
    fs_meta->crtime = tsk_getu32(fs->endian, dino_buf->di_crtime);

    fs_meta->mtime_nano = tsk_getu64(fs->endian, dino_buf->di_mtime);
    fs_meta->atime_nano = tsk_getu64(fs->endian, dino_buf->di_atime);
    fs_meta->ctime_nano = tsk_getu64(fs->endian, dino_buf->di_ctime);
    fs_meta->crtime_nano = tsk_getu64(fs->endian, dino_buf->di_crtime);

    fs_meta->format = dino_buf->di_format;
    switch(fs_meta->format) {
        case XFS_DINODE_FMT_DEV: break;
        case XFS_DINODE_FMT_LOCAL:
        {
            __int128 *dent_ptr;
            dent_ptr = (__int128 *) fs_meta->content_ptr;
            for(int i=0;i<21;i++){
                dent_ptr[i] = tsk_getu64(TSK_LIT_ENDIAN, &dino_buf->di_datafork[i][8]);
                dent_ptr[i] <<= 64;
                dent_ptr[i] |= tsk_getu64(TSK_LIT_ENDIAN, &dino_buf->di_datafork[i][0]);
            }  
        }
        break;
        case XFS_DINODE_FMT_EXTENTS:
        {
            __int128 *extents_ptr;
            extents_ptr = (__int128 *) fs_meta->content_ptr;
            for(int i=0;i<21;i++){
                extents_ptr[i] = tsk_getu64(TSK_LIT_ENDIAN, &dino_buf->di_datafork[i][8]);
                extents_ptr[i] <<= 64;
                extents_ptr[i] |= tsk_getu64(TSK_LIT_ENDIAN, &dino_buf->di_datafork[i][0]);
            }  
        }
        break;
        case XFS_DINODE_FMT_BTREE:
        {
            tsk_fprintf(stderr, "xfs_dinode_copy: B+tree is not implemented yet\n");
            exit(-1);
        }
        break;
        default:
        {
            tsk_fprintf(stderr, "Invalid format\n");
            exit(-1);
        }
        break;
    }

    return 0;
}

/** \internal
 */
uint8_t
tsk_fs_xfs_dinode_core_lookup(TSK_FS_INFO * fs, TSK_FS_FILE * a_fs_file,
    TSK_INUM_T inum)
{
    XFS_INFO *xfs = (XFS_INFO *) fs;
    xfs_dinode_core *dino_buf = NULL;
    unsigned int size = 0;

    if (a_fs_file == NULL) {
        tsk_error_set_errno(TSK_ERR_FS_ARG);
        tsk_error_set_errstr("xfs_dinode_core_lookup: fs_file is NULL");
        return 1;
    }

    if (a_fs_file->meta == NULL) {
        if ((a_fs_file->meta =
                tsk_fs_meta_alloc(XFS_FILE_CONTENT_LEN)) == NULL)
            return 1;
    }
    else {
        tsk_fs_meta_reset(a_fs_file->meta);
    }
    size =
        xfs->inode_size >
        sizeof(xfs_dinode_core) ? xfs->inode_size : sizeof(xfs_dinode_core);
    if ((dino_buf = (xfs_dinode_core *) tsk_malloc(size)) == NULL) {
        return 1;
    }

    if (xfs_dinode_load(xfs, inum, dino_buf)) {
        free(dino_buf);
        return 1;
    }

    if (xfs_dinode_copy(xfs, a_fs_file->meta, inum, dino_buf)) {
        free(dino_buf);
        return 1;
    }

    free(dino_buf);
    return 0;
}


/** \internal
 * Print details about the file system to a file handle. 
 *
 * @param a_fs File system to print details on
 * @param hFile File handle to print text to
 * 
 * @returns 1 on error and 0 on success
 */
uint8_t
tsk_fs_xfs_fsstat(TSK_FS_INFO * fs, FILE * hFile)
{

    XFS_INFO *xfs = (XFS_INFO *) fs;
    xfs_sb *sb = xfs->fs;

    // clean up any error messages that are lying around
    tsk_error_reset();

    tsk_fprintf(hFile, "FILE SYSTEM INFORMATION\n");
    tsk_fprintf(hFile, "---------------------------------------------------\n");
    tsk_fprintf(hFile, "File System Type: XFS");
    
    tsk_fprintf(hFile, "magicnum : %"PRIx32"\n",tsk_getu32(fs->endian,sb->sb_magicnum));
    tsk_fprintf(hFile, "blocksize : %"PRIx32"\n",tsk_getu32(fs->endian,sb->sb_blocksize));
    tsk_fprintf(hFile, "dblocks : %"PRIx64"\n",tsk_getu64(fs->endian,sb->sb_dblocks));
    tsk_fprintf(hFile, "rblocks : %"PRIx64"\n",tsk_getu64(fs->endian,sb->sb_rblocks));
    tsk_fprintf(hFile, "rextents : %"PRIx64"\n",tsk_getu64(fs->endian,sb->sb_rextents));
    tsk_fprintf(hFile, "uuid : %" PRIx64 "%" PRIx64 "\n",
            tsk_getu64(fs->endian, &sb->sb_uuid[0]), tsk_getu64(fs->endian, &sb->sb_uuid[8]));
    tsk_fprintf(hFile, "logstart : %"PRIx64"\n",tsk_getu64(fs->endian,sb->sb_logstart));
    tsk_fprintf(hFile, "rootino : %"PRIx64"\n",tsk_getu64(fs->endian,sb->sb_rootino));
    tsk_fprintf(hFile, "rbmino : %"PRIx64"\n",tsk_getu64(fs->endian,sb->sb_rbmino));
    tsk_fprintf(hFile, "rsumino : %"PRIx64"\n",tsk_getu64(fs->endian,sb->sb_rsumino));
    tsk_fprintf(hFile, "rextsize : %"PRIx32"\n",tsk_getu32(fs->endian,sb->sb_rextsize));
    tsk_fprintf(hFile, "agblocks : %"PRIx32"\n",tsk_getu32(fs->endian,sb->sb_agblocks));
    tsk_fprintf(hFile, "agcount : %"PRIx32"\n",tsk_getu32(fs->endian,sb->sb_agcount));
    tsk_fprintf(hFile, "rbmblocks : %"PRIx32"\n",tsk_getu32(fs->endian,sb->sb_rbmblocks));
    tsk_fprintf(hFile, "logblocks : %"PRIx32"\n",tsk_getu32(fs->endian,sb->sb_logblocks));
    tsk_fprintf(hFile, "versionnum : %"PRIx16"\n",tsk_getu16(fs->endian,sb->sb_versionnum));
    tsk_fprintf(hFile, "agblocks : %"PRIx32"\n",tsk_getu32(fs->endian,sb->sb_agblocks));
    tsk_fprintf(hFile, "agcount : %"PRIx32"\n",tsk_getu32(fs->endian,sb->sb_agcount));
    tsk_fprintf(hFile, "sectsize : %"PRIx16"\n",tsk_getu16(fs->endian,sb->sb_sectsize));
    tsk_fprintf(hFile, "inodesize : %"PRIx16"\n",tsk_getu16(fs->endian,sb->sb_inodesize));
    tsk_fprintf(hFile, "inopblock : %"PRIx16"\n",tsk_getu16(fs->endian,sb->sb_inopblock));
    tsk_fprintf(hFile, "icount : %"PRIx64"\n",tsk_getu64(fs->endian,sb->sb_icount));
    tsk_fprintf(hFile, "ifree : %"PRIx64"\n",tsk_getu64(fs->endian,sb->sb_ifree));

    return 0;
}


/** \internal
 */
TSK_FS_ATTR_TYPE_ENUM
tsk_fs_xfs_get_default_attr_type(const TSK_FS_FILE * a_fs_file)
{
    return TSK_FS_ATTR_TYPE_DEFAULT;
}


/** \internal
 */
uint8_t
tsk_fs_xfs_make_data_run(TSK_FS_FILE * fs_file)
{
    TSK_OFF_T length = 0;
    TSK_OFF_T read_b = 0;
    TSK_FS_ATTR *fs_attr;
    TSK_FS_META *fs_meta = fs_file->meta;
    TSK_FS_INFO *fs = fs_file->fs_info;
    XFS_INFO *xfs = (XFS_INFO *) fs;

    // clean up any error messages that are lying around
    tsk_error_reset();

    if (tsk_verbose)
        tsk_fprintf(stderr,
            "xfs_make_data_run: Processing file %" PRIuINUM "\n",
            fs_meta->addr);

    // see if we have already loaded the runs
    if ((fs_meta->attr != NULL)
        && (fs_meta->attr_state == TSK_FS_META_ATTR_STUDIED)) {
        return 0;
    }
    
    if (fs_meta->attr_state == TSK_FS_META_ATTR_ERROR) {
        return 1;
    }

    // not sure why this would ever happen, but...
    if (fs_meta->attr != NULL) {
        tsk_fs_attrlist_markunused(fs_meta->attr);
    }
    else {
        fs_meta->attr = tsk_fs_attrlist_alloc();
    }

    if (TSK_FS_TYPE_ISXFS(fs->ftype)==0) {
        tsk_error_set_errno(TSK_ERR_FS_INODE_COR);
        tsk_error_set_errstr
            ("xfs_make_run: this is not xfs: %x",
            fs->ftype);
        return 1;
    }

    length = roundup(fs_meta->size, fs->block_size);

    if ((fs_attr =
            tsk_fs_attrlist_getnew(fs_meta->attr,
                TSK_FS_ATTR_NONRES)) == NULL) {
        return 1;
    }

    // initialize the data run
    if (tsk_fs_attr_set_run(fs_file, fs_attr, NULL, NULL,
            TSK_FS_ATTR_TYPE_DEFAULT, TSK_FS_ATTR_ID_DEFAULT,
            fs_meta->size, fs_meta->size, roundup(fs_meta->size,
                fs->block_size), 0, 0)) {
        return 1;
    }

    int i;
    if(fs_meta->type == TSK_FS_META_TYPE_REG
         && fs_meta->format == XFS_DINODE_FMT_EXTENTS) { 
        __int128 *extent = (__int128 *)fs_meta->content_ptr;
        for(i=0;i<21;i++) {
            TSK_FS_ATTR_RUN *data_run;
            uint64_t high = (extent[i] >> 64) & 0xffffffffffffffff;
            uint64_t low = extent[i] & 0xffffffffffffffff;
            high = tsk_getu64(TSK_BIG_ENDIAN, &high);
            low = tsk_getu64(TSK_BIG_ENDIAN, &low);
            extent[i] = low;
            extent[i] << 64;
            extent[i] |= high;
            uint8_t br_state = (extent[i] >> 127);
            uint64_t br_startoff = (extent[i] >> 73) & 0x3fffffffffffff;
            uint64_t br_startblock = (extent[i] >> 21) & 0xfffffffffffff;
            uint64_t br_blockcount = (extent[i]) & 0x1fffff;

            if(br_state == 0 && br_startoff == 0 && br_startblock == 0 && br_blockcount ==0)
                break;

            uint32_t agblocks = tsk_getu32(TSK_BIG_ENDIAN, xfs->fs->sb_agblocks);
            uint64_t AG_number = br_startblock/agblocks;
            br_startblock -= ((1<<xfs->fs->sb_agblklog) - agblocks)*(AG_number);
            if(tsk_verbose) {
                tsk_fprintf(stderr, "\nAG_number: %x", AG_number); 
                tsk_fprintf(stderr, "\nbr_state : %x, br_startoff : %x, br_startblock : %x, br_blockcount : %x\n",
                    br_state, br_startoff, br_startblock, br_blockcount);
            }

            // make a non-resident run
            data_run = tsk_fs_attr_run_alloc();
            if (data_run == NULL)
                return -1;
            data_run->offset = 0;
            data_run->addr = br_startblock + br_startoff*AG_number*agblocks;
            data_run->len = br_blockcount;
            tsk_fs_attr_append_run(fs, fs_attr, data_run);
        }
    }

    fs_meta->attr_state = TSK_FS_META_ATTR_STUDIED;

    return 0;
}



/** \internal
 */
void
tsk_fs_xfs_close(TSK_FS_INFO * a_fs)
{
    a_fs->tag = 0;
    tsk_fs_free(a_fs);
}

/************* BLOCKS *************/

/** \internal
 */
TSK_FS_BLOCK_FLAG_ENUM
tsk_fs_xfs_block_getflags(TSK_FS_INFO * a_fs, TSK_DADDR_T a_addr)
{
    return TSK_FS_BLOCK_FLAG_ALLOC | TSK_FS_BLOCK_FLAG_CONT;
}


/** \internal
 *
 * return 1 on error and 0 on success
 */
uint8_t
tsk_fs_xfs_block_walk(TSK_FS_INFO * fs, TSK_DADDR_T a_start_blk,
    TSK_DADDR_T a_end_blk, TSK_FS_BLOCK_WALK_FLAG_ENUM a_flags,
    TSK_FS_BLOCK_WALK_CB a_action, void *a_ptr)
{
    TSK_FS_BLOCK *fs_block;
    TSK_DADDR_T addr;

    // clean up any error messages that are lying around
    tsk_error_reset();

    /*
     * Sanity checks.
     */
    if (a_start_blk < fs->first_block || a_start_blk > fs->last_block) {
        tsk_error_reset();
        tsk_error_set_errno(TSK_ERR_FS_WALK_RNG);
        tsk_error_set_errstr("xfs_block_walk: Start block number: %"
            PRIuDADDR, a_start_blk);
        return 1;
    }

    if (a_end_blk < fs->first_block || a_end_blk > fs->last_block
        || a_end_blk < a_start_blk) {
        tsk_error_reset();
        tsk_error_set_errno(TSK_ERR_FS_WALK_RNG);
        tsk_error_set_errstr("xfs_block_walk: Last block number: %"
            PRIuDADDR, a_end_blk);
        return 1;
    }

    /* Sanity check on a_flags -- make sure at least one ALLOC is set */
    if (((a_flags & TSK_FS_BLOCK_WALK_FLAG_ALLOC) == 0) &&
        ((a_flags & TSK_FS_BLOCK_WALK_FLAG_UNALLOC) == 0)) {
        a_flags |=
            (TSK_FS_BLOCK_WALK_FLAG_ALLOC |
            TSK_FS_BLOCK_WALK_FLAG_UNALLOC);
    }

    /* All swap has is allocated blocks... exit if not wanted */
    if (!(a_flags & TSK_FS_BLOCK_FLAG_ALLOC)) {
        return 0;
    }

    if ((fs_block = tsk_fs_block_alloc(fs)) == NULL) {
        return 1;
    }

    for (addr = a_start_blk; addr <= a_end_blk; addr++) {
        int retval;

        if (tsk_fs_block_get(fs, fs_block, addr) == NULL) {
            tsk_error_set_errstr2("xfs_block_walk: Block %" PRIuDADDR,
                addr);
            tsk_fs_block_free(fs_block);
            return 1;
        }

        retval = a_action(fs_block, a_ptr);
        if (retval == TSK_WALK_STOP) {
            break;
        }
        else if (retval == TSK_WALK_ERROR) {
            tsk_fs_block_free(fs_block);
            return 1;
        }
    }

    /*
     * Cleanup.
     */
    tsk_fs_block_free(fs_block);
    return 0;
}


/************ META / FILES ************/


/** \internal
 */
uint8_t
tsk_fs_xfs_dinode_core_walk(TSK_FS_INFO * a_fs, TSK_INUM_T a_start_inum,
    TSK_INUM_T a_end_inum, TSK_FS_META_FLAG_ENUM a_flags,
    TSK_FS_META_WALK_CB a_action, void *a_ptr)
{
    tsk_error_reset();
    tsk_error_set_errno(TSK_ERR_FS_UNSUPFUNC);
    tsk_error_set_errstr("Illegal analysis method for %s data ",
        tsk_fs_type_toname(a_fs->ftype));
    return 1;
}


/** \internal
 */
/**
* Print details on a specific file to a file handle.
*
* @param fs File system file is located in
* @param hFile File handle to print text to
* @param inum Address of file in file system
* @param numblock The number of blocks in file to force print (can go beyond file size)
* @param sec_skew Clock skew in seconds to also print times in
*
* @returns 1 on error and 0 on success
*/
// yaffs.cpp is similar
uint8_t
tsk_fs_xfs_istat(TSK_FS_INFO * a_fs, TSK_FS_ISTAT_FLAG_ENUM istat_flags, FILE * hFile, TSK_INUM_T inum,
    TSK_DADDR_T numblock, int32_t sec_skew)
{
    tsk_error_reset();
    tsk_error_set_errno(TSK_ERR_FS_UNSUPFUNC);
    tsk_error_set_errstr("Illegal analysis method for %s data ",
        tsk_fs_type_toname(a_fs->ftype));
    return 1;
}




/******** JOURNAL **********/

/** \internal
 */
uint8_t
tsk_fs_xfs_jopen(TSK_FS_INFO * a_fs, TSK_INUM_T inum)
{
    tsk_error_reset();
    tsk_error_set_errno(TSK_ERR_FS_UNSUPFUNC);
    tsk_error_set_errstr("Illegal analysis method for %s data ",
        tsk_fs_type_toname(a_fs->ftype));
    return 1;
}

/** \internal
 */
uint8_t
tsk_fs_xfs_jentry_walk(TSK_FS_INFO * a_fs, int a_flags,
    TSK_FS_JENTRY_WALK_CB a_action, void *a_ptr)
{
    tsk_error_reset();
    tsk_error_set_errno(TSK_ERR_FS_UNSUPFUNC);
    tsk_error_set_errstr("Illegal analysis method for %s data ",
        tsk_fs_type_toname(a_fs->ftype));
    return 1;
}


/** \internal
 */
uint8_t
tsk_fs_xfs_jblk_walk(TSK_FS_INFO * a_fs, TSK_INUM_T start, TSK_INUM_T end,
    int a_flags, TSK_FS_JBLK_WALK_CB a_action, void *a_ptr)
{
    tsk_error_reset();
    tsk_error_set_errno(TSK_ERR_FS_UNSUPFUNC);
    tsk_error_set_errstr("Illegal analysis method for %s data ",
        tsk_fs_type_toname(a_fs->ftype));
    return 1;
}

int
tsk_fs_xfs_name_cmp(TSK_FS_INFO * a_fs_info, const char *s1,
    const char *s2)
{
    return strcmp(s1, s2);
}
/** \internal
 * Open part of a disk image as a raw file system -- which basically means that it has no file system structure.
 * The data is considered to be in 512-byte sectors. 
 *
 * @param img_info Disk image to analyze
 * @param offset Byte offset where "file system" starts
 * @returns NULL on error
 */
// It is copied from ext2fs.c
TSK_FS_INFO *
xfs_open(TSK_IMG_INFO * img_info, TSK_OFF_T offset,
            TSK_FS_TYPE_ENUM ftype, uint8_t test)
{
    XFS_INFO *xfs;
    TSK_OFF_T len;
    TSK_FS_INFO *fs;
    ssize_t cnt;

    // clean up any error messages that are lying around
    tsk_error_reset();

    if (TSK_FS_TYPE_ISXFS(ftype) == 0) {
        tsk_error_reset();
        tsk_error_set_errno(TSK_ERR_FS_ARG);
        tsk_error_set_errstr("Invalid FS Type in xfs_open");
        return NULL;
    }

    if (img_info->sector_size == 0) {
        tsk_error_reset();
        tsk_error_set_errno(TSK_ERR_FS_ARG);
        tsk_error_set_errstr("xfs_open: sector size is 0");
        return NULL;
    }

    if ((xfs = (XFS_INFO *) tsk_fs_malloc(sizeof(*xfs))) == NULL)
        return NULL;

    fs = &(xfs->fs_info);

    /* All we need to set are the block sizes and max block size etc. */
    fs->img_info = img_info;
    fs->offset = offset;

    fs->ftype = ftype;
    fs->duname = "Block";   // duname is "Block"
    fs->flags = 0;
    fs->tag = TSK_FS_INFO_TAG;
    fs->endian = TSK_BIG_ENDIAN;    // XFS is Big Endian

    len = sizeof(xfs_sb);
    if ((xfs->fs = (xfs_sb *) tsk_malloc(len)) == NULL) {
        fs->tag = 0;
        return NULL;
    }

    cnt = tsk_fs_read(fs, 0, (char *)xfs->fs, len);
    if (cnt != len) {
        if (cnt >= 0) {
            tsk_error_reset();
            tsk_error_set_errno(TSK_ERR_FS_READ);
        }
        tsk_error_set_errstr2("xfs_open: superblock");
        fs->tag = 0;
        return NULL;
    }

    /*
     * Verify we are looking at an XFS image
     */
    if (tsk_fs_guessu32(fs, xfs->fs->sb_magicnum, XFS_SB_MAGIC)) {
        fs->tag = 0;
        tsk_error_reset();
        tsk_error_set_errno(TSK_ERR_FS_MAGIC);
        tsk_error_set_errstr("not an XFS file system (magic)");
        return NULL;
    }

    /*
     * Calculate the meta data info
     */
    fs->inum_count = tsk_getu64(fs->endian, xfs->fs->sb_icount) 
                    - tsk_getu64(fs->endian, xfs->fs->sb_ifree);
    fs->root_inum = tsk_getu64(fs->endian, xfs->fs->sb_rootino);
    fs->first_inum = fs->root_inum;
    fs->last_inum = fs->first_inum + fs->inum_count - 1;

    xfs->inode_size = tsk_getu16(fs->endian, xfs->fs->sb_inodesize);

    fs->dev_bsize = img_info->sector_size;

    fs->first_block = 0;
    fs->block_count = tsk_getu64(fs->endian,xfs->fs->sb_dblocks);
    fs->last_block = fs->last_block_act = fs->block_count - 1;
    fs->dev_bsize = img_info->sector_size;

    fs->block_size = tsk_getu32(fs->endian, xfs->fs->sb_blocksize);

    // determine the last block we have in this image
    if ((TSK_DADDR_T) ((img_info->size - offset) / fs->block_size) <
        fs->block_count)
        fs->last_block_act =
            (img_info->size - offset) / fs->block_size - 1;

    /* Volume ID */
    for (fs->fs_id_used = 0; fs->fs_id_used < 16; fs->fs_id_used++) {
        fs->fs_id[fs->fs_id_used] = xfs->fs->sb_uuid[fs->fs_id_used];
    }

    /* Pointer to functions */
    fs->close = tsk_fs_xfs_close;
    fs->fsstat = tsk_fs_xfs_fsstat;

    fs->block_walk = tsk_fs_xfs_block_walk;
    fs->block_getflags = tsk_fs_xfs_block_getflags;

    fs->inode_walk = tsk_fs_xfs_dinode_core_walk;
    fs->file_add_meta = tsk_fs_xfs_dinode_core_lookup;
    fs->istat = tsk_fs_xfs_istat;

    fs->get_default_attr_type = tsk_fs_xfs_get_default_attr_type;
    fs->load_attrs = tsk_fs_xfs_make_data_run;

    fs->file_add_meta = tsk_fs_xfs_dinode_core_lookup;        // important!
    fs->dir_open_meta = tsk_fs_xfs_dir_open_meta;   // important!
    fs->name_cmp = tsk_fs_xfs_name_cmp;

    fs->jblk_walk = tsk_fs_xfs_jblk_walk;
    fs->jentry_walk = tsk_fs_xfs_jentry_walk;
    fs->jopen = tsk_fs_xfs_jopen;
    fs->journ_inum = 0;

    return (fs);
}