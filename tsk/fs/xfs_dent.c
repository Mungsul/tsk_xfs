/*
** The Sleuth Kit
**
** Brian Carrier [carrier <at> sleuthkit [dot] org]
** Copyright (c) 2006-2011 Brian Carrier, Basis Technology.  All Rights reserved
** Copyright (c) 2004-2005 Brian Carrier.  All rights reserved 
**
**
** This software is distributed under the Common Public License 1.0
** 
*/

#include "tsk_fs_i.h"
#include "tsk_xfs.h"

/************ DIR **************/

/* get_xfs_inode_num - convert absolute inode number to real inode number
 *
 * Returns TSK_ERR on error and real_inode(real inode number) on success
 *
 */
uint64_t
get_xfs_inode_num(XFS_INFO* xfs, uint64_t abs_inode) {
    xfs_sb* sb = xfs->fs;

    uint8_t inopblog_bits = sb->sb_inopblog;                    // for sec_num
    uint8_t agblklog_bits = sb->sb_agblklog;                    // for blk_num
    uint8_t AG_num_bits = 32 - inopblog_bits - agblklog_bits;   // for AG_num

    uint32_t sec_mask = 0;  // for sec_num
    uint32_t blk_mask = 0;  // for blk_num
    uint32_t AG_mask = 0;   // for AG_num
    int bit_mask = 1;       // for masking
    int i;

    for(i=0;i<inopblog_bits;i++) {
        sec_mask |= bit_mask;
        bit_mask<<=1;
    }
    for(i=0;i<agblklog_bits;i++) {
        blk_mask |= bit_mask;
        bit_mask<<=1;
    }
    for(i=0;i<AG_num_bits;i++) {
        AG_mask |= bit_mask;
        bit_mask<<=1;
    }

    uint32_t rel_sec_num = abs_inode & sec_mask;
    uint32_t rel_blk_num = (abs_inode & blk_mask) >> (inopblog_bits); // relative block number
    uint32_t AG_num = (abs_inode & AG_mask) >> (inopblog_bits + agblklog_bits);   // AG number
    
    // inode number per AG * AG number + inode number per block * block number + inode number per sector * sector number
    uint32_t agblocks = tsk_getu32(TSK_BIG_ENDIAN, sb->sb_agblocks);
    uint32_t blocksize = tsk_getu32(TSK_BIG_ENDIAN, sb->sb_blocksize);
    uint32_t sectsize = tsk_getu16(TSK_BIG_ENDIAN, sb->sb_sectsize);
    uint32_t inodesize = tsk_getu16(TSK_BIG_ENDIAN, sb->sb_inodesize);

    uint64_t real_inode = (agblocks*blocksize/inodesize) * AG_num + (blocksize/inodesize)*rel_blk_num + (sectsize/inodesize)*rel_sec_num;

    return real_inode;
}

/* xfs_dent_parse_block - parse directory entries in block directory
 *
 * Returns TSK_ERR on error and TSK_OK on success
 *
 */
TSK_RETVAL_ENUM
xfs_dent_parse_block(TSK_FS_INFO * a_fs, XFS_INFO * xfs, TSK_FS_DIR *fs_dir, unsigned char *contents, uint16_t len)
{
    TSK_FS_INFO *fs = &(xfs->fs_info);
    xfs_sb* sb = xfs->fs;

    int cursor;
    int dentnum = -2;
    uint32_t inode;
    char *dirPtr;
    TSK_FS_NAME *fs_name;

    /*
     * Verify we are looking at an block directory
     */
    if (tsk_getu32(TSK_BIG_ENDIAN, &contents[0]) == XFS_DIR3_BLOCK_MAGIC) {
        if(tsk_verbose) {
            tsk_fprintf(stderr, "block directory(magic)\n");
        }
    }
    else {
        if(tsk_verbose) {
            tsk_fprintf(stderr, "not implemented format (magic %x)\n", tsk_getu32(TSK_BIG_ENDIAN, &contents[0]));    
        }
        return TSK_ERR;
    }

    if ((fs_name = tsk_fs_name_alloc(XFS_MAXNAMLEN+1, 0)) == NULL)
        return TSK_ERR;

    cursor = 0x40;
    while (cursor < len) {
        TSK_FS_NAME *fs_name = tsk_fs_name_alloc(XFS_MAXNAMLEN+1, 0);
        if(fs_name == NULL) {
            return TSK_ERR;
        }
        uint64_t abs_inode = tsk_getu64(a_fs->endian, &contents[cursor]); cursor+=8;
        uint8_t namelen = contents[cursor]; cursor++;
        memcpy(fs_name->name, &contents[cursor], namelen); cursor+=namelen;
        uint8_t file_type = contents[cursor]; cursor++;

        cursor = XFS_DIRSIZ_lcl(len);

        switch(file_type) {
            case XFS_DE_REG: fs_name->type = TSK_FS_NAME_TYPE_REG; break;
            case XFS_DE_DIR: fs_name->type = TSK_FS_NAME_TYPE_DIR; break;
            case XFS_DE_CHR: fs_name->type = TSK_FS_NAME_TYPE_CHR; break;
            case XFS_DE_BLK: fs_name->type = TSK_FS_NAME_TYPE_BLK; break;
            case XFS_DE_FIFO: fs_name->type = TSK_FS_NAME_TYPE_FIFO; break;
            case XFS_DE_SOCK: fs_name->type = TSK_FS_NAME_TYPE_SOCK; break;
            case XFS_DE_LNK: fs_name->type = TSK_FS_NAME_TYPE_LNK; break;
            default: fs_name->type = TSK_FS_NAME_TYPE_UNDEF; break;
        }

        fs_name->meta_addr = get_xfs_inode_num(xfs, abs_inode);
        fs_name->flags = 0;
        fs_name->flags = TSK_FS_NAME_FLAG_ALLOC;
        
        if(dentnum>=0) {
            if (tsk_fs_dir_add(fs_dir, fs_name)) {
                tsk_fs_name_free(fs_name);
                return TSK_ERR;
            }
        }
        dentnum++;

        if(tsk_verbose)
            tsk_fprintf(stderr, "\ninode %08x, namelen : %02x, type : %02x, name : %s",
             fs_name->meta_addr, namelen, file_type, fs_name->name);
        
    }

    tsk_fs_name_free(fs_name);
    return TSK_OK;
}


/** \internal
 */
TSK_RETVAL_ENUM
tsk_fs_xfs_dir_open_meta(TSK_FS_INFO * a_fs, TSK_FS_DIR ** a_fs_dir,
    TSK_INUM_T a_addr)
{
    XFS_INFO *xfs = (XFS_INFO *) a_fs;
    xfs_sb *sb = xfs->fs;               // for using sb_inopblog, sb_agblklog
    char *dirbuf;
    TSK_OFF_T size;
    TSK_FS_DIR *fs_dir;

    /* If we get corruption in one of the blocks, then continue processing.
     * retval_final will change when corruption is detected.  Errors are
     * returned immediately. */
    TSK_RETVAL_ENUM retval_tmp;
    TSK_RETVAL_ENUM retval_final = TSK_OK;

    uint16_t type;
    uint8_t format;

    if (a_addr < a_fs->first_inum) {
        tsk_error_reset();
        tsk_error_set_errno(TSK_ERR_FS_WALK_RNG);
        tsk_error_set_errstr("xfs_dir_open_meta: inode value: %"
            PRIuINUM "\n", a_addr);
        return TSK_ERR;
    }
    else if (a_fs_dir == NULL) {
        tsk_error_reset();
        tsk_error_set_errno(TSK_ERR_FS_ARG);
        tsk_error_set_errstr
            ("xfs_dir_open_meta: NULL fs_attr argument given");
        return TSK_ERR;
    }

    if (tsk_verbose) {
        tsk_fprintf(stderr,
            "tsk_fs_xfs_dir_open_meta: Processing directory %" PRIuINUM
            "\n", a_addr);
    }

    fs_dir = *a_fs_dir;
    if (fs_dir) {
        tsk_fs_dir_reset(fs_dir);
        fs_dir->addr = a_addr;
    }
    else {
        if ((*a_fs_dir = fs_dir =
                tsk_fs_dir_alloc(a_fs, a_addr, 128)) == NULL) {
            return TSK_ERR;
        }
    }

    if ((fs_dir->fs_file =
            tsk_fs_file_open_meta(a_fs, NULL, a_addr)) == NULL) {
        tsk_error_reset();
        tsk_error_errstr2_concat("- xfs_dir_open_meta");
        return TSK_COR;
    }

    // We only read in and process a single block at a time
    if ((dirbuf = tsk_malloc((size_t)a_fs->block_size)) == NULL) {
        return TSK_ERR;
    }

    type = fs_dir->fs_file->meta->type;
    format = fs_dir->fs_file->meta->format;

    if(type == TSK_FS_META_TYPE_DIR 
        && format == XFS_DINODE_FMT_LOCAL) { // short form direcotry
        
        unsigned char *contents = (unsigned char*)fs_dir->fs_file->meta->content_ptr;
        uint8_t dent_count = contents[0];
        uint8_t i8count = contents[1];
        uint32_t parent_inode = tsk_getu32(TSK_BIG_ENDIAN, &contents[2]);
        if(tsk_verbose)
            tsk_fprintf(stderr,"\ncount : %02X, i8count : %02X, parent : %08X", dent_count, i8count, parent_inode);

        int cursor = 6;
        for(int i=0;i<dent_count;i++) {
            TSK_FS_NAME *fs_name = tsk_fs_name_alloc(XFS_MAXNAMLEN+1, 0);
            if(fs_name == NULL) {
                return TSK_ERR;
            }
            uint8_t namelen = contents[cursor]; cursor++;
            uint16_t offset = tsk_getu16(a_fs->endian, &contents[cursor]); cursor+=2; 
            memcpy(fs_name->name, &contents[cursor], namelen); cursor+=namelen;
            uint8_t file_type = contents[cursor]; cursor++;
            uint64_t abs_inode = tsk_getu32(a_fs->endian, &contents[cursor]); cursor+=4;
            
            switch(file_type) {
                case XFS_DE_REG: fs_name->type = TSK_FS_NAME_TYPE_REG; break;
                case XFS_DE_DIR: fs_name->type = TSK_FS_NAME_TYPE_DIR; break;
                case XFS_DE_CHR: fs_name->type = TSK_FS_NAME_TYPE_CHR; break;
                case XFS_DE_BLK: fs_name->type = TSK_FS_NAME_TYPE_BLK; break;
                case XFS_DE_FIFO: fs_name->type = TSK_FS_NAME_TYPE_FIFO; break;
                case XFS_DE_SOCK: fs_name->type = TSK_FS_NAME_TYPE_SOCK; break;
                case XFS_DE_LNK: fs_name->type = TSK_FS_NAME_TYPE_LNK; break;
                default: fs_name->type = TSK_FS_NAME_TYPE_UNDEF; break;
            }

            fs_name->meta_addr = get_xfs_inode_num(xfs, abs_inode);
            fs_name->flags = 0;
            fs_name->flags = TSK_FS_NAME_FLAG_ALLOC;

            if (tsk_fs_dir_add(fs_dir, fs_name)) {
                tsk_fs_name_free(fs_name);
                return TSK_ERR;
            }
            if(tsk_verbose)
                tsk_fprintf(stderr, "\nnamelen : %02X, offset : %04X, name : %s, type : %02X, abs_inode %08X", namelen, offset, fs_name->name, file_type, fs_name->meta_addr);
        }
    }
    else if(type == TSK_FS_META_TYPE_DIR 
        && format == XFS_DINODE_FMT_EXTENTS) { // block directory

        __int128 *extent = (__int128 *)(unsigned char*)fs_dir->fs_file->meta->content_ptr;;
        for(int i=0;i<21;i++) {
            uint64_t high = (extent[i] >> 64) & 0xffffffffffffffff;
            uint64_t low = extent[i] & 0xffffffffffffffff;
            high = tsk_getu64(TSK_BIG_ENDIAN, &high);
            low = tsk_getu64(TSK_BIG_ENDIAN, &low);
            extent[i] = low;
            extent[i] << 64;
            extent[i] |= high;
            uint8_t br_state = (extent[i] >> 127);
            uint64_t br_startoff = (extent[i] >> 73) & 0x3fffffffffffff;
            uint64_t br_startblock = (extent[i] >> 21) & 0xfffffffffffff;
            uint64_t br_blockcount = (extent[i]) & 0x1fffff;

            if(br_state == 0 && br_startoff == 0 && br_startblock == 0 && br_blockcount ==0)
                break;

            uint32_t agblocks = tsk_getu32(TSK_BIG_ENDIAN, xfs->fs->sb_agblocks);
            uint64_t AG_number = br_startblock/agblocks;
            br_startblock -= ((1<<xfs->fs->sb_agblklog) - agblocks)*(AG_number);
            if(tsk_verbose) {
                tsk_fprintf(stderr, "\nAG_number: %X", AG_number); 
                tsk_fprintf(stderr, "\nbr_state : %X, br_startoff : %X, br_startblock : %X, br_blockcount : %X\n",
                    br_state, br_startoff, br_startblock, br_blockcount);
            }

            /* access to the block directory */
            unsigned char* blkd_ptr;                    // pointer to the block diredcotry
            uint32_t blksize = a_fs->block_size;        // block size
            TSK_OFF_T a_off = (br_startoff*AG_number*agblocks + br_startblock)*blksize;    // start address of block directory in byte

            if ((blkd_ptr = (unsigned char *) tsk_malloc(blksize)) == NULL) {
                return TSK_ERR;
            }

            if(tsk_img_read(a_fs->img_info, a_off, blkd_ptr, blksize)==-1) {
                return TSK_ERR;
            }

            uint16_t len = tsk_getu16(TSK_BIG_ENDIAN, blkd_ptr+0x30);

            if(xfs_dent_parse_block(a_fs, xfs, fs_dir, blkd_ptr, len) == TSK_ERR) {
                return TSK_ERR;
            }

            free(blkd_ptr);
        }
    }

    free(dirbuf);
    return retval_final;
}
